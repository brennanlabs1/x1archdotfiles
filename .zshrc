# Lines configured by zsh-newuser-install
autoload -U colors && colors

HISTFILE=~/.cache/zsh/histfile
HISTSIZE=1000
SAVEHIST=1000
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/brennanlabs/.zshrc'

autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist

compinit
_comp_options+=(globdots)

# End of lines added by compinstall

alias v='nvim'
alias vim='nvim'

alias config='/usr/bin/git --git-dir=/home/brennanlabs/x1archdotfiles --work-tree=/home/brennanlabs'


eval "$(starship init zsh)"
