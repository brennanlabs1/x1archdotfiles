set nocompatible
filetype off

:set formatoptions-=cro

call plug#begin('~/.config/nvim/plugged')

Plug 'morhetz/gruvbox'
Plug 'sheerun/vim-polyglot'

Plug 'itchyny/lightline.vim'

Plug 'norcalli/nvim-colorizer.lua'

Plug 'ap/vim-css-color'

Plug 'gko/vim-coloresque'

call plug#end()

execute "set t_8f=\e[38;2;%lu;%lu;%lum"
execute "set t_8b=\e[48;2;%lu;%lu;%lum"
if (has("termguicolors"))
	set termguicolors
endif

lua require'colorizer'.setup()

set path+=**
set wildmenu
set incsearch
set hidden
set nobackup
set noswapfile
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set number relativenumber
syntax enable
set splitbelow splitright

colorscheme gruvbox
set background=dark

let mapleader = " "
let g:netrw_browse_split=2
let g:netrw_banner = 0
let g:netrw_winsize = 25

map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

vnoremap <C-c> "*y :let @+=@*<CR>
map <C-v> "+P

